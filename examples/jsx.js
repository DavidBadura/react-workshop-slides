import React from 'react';

class HelloWorld extends React.Component {
  render() {
    return (
      React.createElement(
        'div',
        { className: 'jumbotron' },
        React.createElement(
          'h1',
          { className: 'display-4' },
          'Hello, world!'
        )
      )
    );
  }
}

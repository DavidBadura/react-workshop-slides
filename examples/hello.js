import React from 'react';

class HelloWorld extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <h1 className="display-4">Hello, world!</h1>
      </div>
    );
  }
}

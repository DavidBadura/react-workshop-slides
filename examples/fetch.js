import React from 'react';

class MoviePage extends React.Component {
  state = {
    data: []
  };

  async componentDidMount() {
    const response = await fetch('http://example.com/movies.json');
    const data = await response.json();

    this.setState({
      data: data
    });
  }

  render() {
    return (
      <ul>
        {this.state.data.map(this.renderMovie)}
      </ul>
    );
  }

  renderMovie(movie) {
    return <li key={movie.id}>{movie.title}</li>;
  }
}

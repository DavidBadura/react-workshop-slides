import React from 'react';

function SuperButton(props) {
  return (
    <div className="row">
      <button onClick={props.onSuperClick}>{props.children}</button>
    </div>
  );
}

class HelloWorld extends React.Component {
  render() {
    return (
      <SuperButton onSuperClick={() => alert('Super Click!')}>
        Hello, world!
      </SuperButton>
    );
  }
}

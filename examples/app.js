import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

function Index() { return <h1>Home</h1>; }
function About() { return <h1>About</h1>; }
function Users() { return <h1>Users</h1>; }

function App() {
  return (
    <Router>
      <div>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about/">About</Link></li>
          <li><Link to="/users/">Users</Link></li>
        </ul>

        <Route path="/" exact component={Index}/>
        <Route path="/about/" component={About}/>
        <Route path="/users/" component={Users}/>
      </div>
    </Router>
  );
}

import React from 'react';

function Light(props) {
  const stateClassName = props.on ? 'light-on' : 'light-off';
  return <div className={'light ' + stateClassName} />;
}

class LightSwitch extends React.Component {
  state = {
    on: false
  };

  press = () => {
    this.setState({
      on: !this.state.on
    });
  };

  render() {
    return (
      <div>
        <Light on={this.state.on}/>
        <button onClick={this.press}>Light Switch</button>
      </div>
    );
  }
}

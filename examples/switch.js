import React from 'react';

class LightSwitch extends React.Component {
  state = {
    on: false
  };

  press = () => {
    this.setState({
      on: !this.state.on
    });
  };

  render() {
    return (
      <button onClick={this.press}>
        {this.state.on ? 'On' : 'Off'}
      </button>
    );
  }
}

import React from 'react';

function Light(props) { /**/ }

function LightSwitch(props) { /**/ }

class Relais extends React.Component {
  state = {
    on: false
  };

  press = () => {
    this.setState({
      on: !this.state.on
    });
  };

  render() {
    return (
      <React.Fragment>
        <Light on={this.state.on}/>
        <LightSwitch onPress={this.press}/>
        <LightSwitch onPress={this.press}/>
      </React.Fragment>
    );
  }
}

import React from 'react';

function Jumbotron(props) {
  return (
    <div className="jumbotron">
      <h1 className="display-4">{props.children}</h1>
    </div>
  );
}

class HelloWorld extends React.Component {
  render() {
    return <Jumbotron>Hello, world!</Jumbotron>;
  }
}

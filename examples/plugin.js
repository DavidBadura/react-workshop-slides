import React from 'react';

class GoogleMap extends React.Component {
  componentDidMount() {
    this.$element = $(this.element);
    this.$element.googleMaps();
  }

  componentWillUnmount() {
    this.$element.googleMaps('destroy');
  }

  render() {
    return <div ref={element => this.element = element}/>;
  }
}

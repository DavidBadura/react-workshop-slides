import React from 'react';

class Jumbotron extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <h1 className="display-4">{this.props.children}</h1>
      </div>
    );
  }
}

class HelloWorld extends React.Component {
  render() {
    return <Jumbotron>Hello, world!</Jumbotron>;
  }
}

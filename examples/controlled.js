import React from 'react';

function Light(props) { /**/ }

function LightSwitch(props) {
  return <button onClick={props.onPress}>Light Switch</button>;
}

class Relais extends React.Component {
  state = {
    on: false
  };

  press = () => {
    this.setState({
      on: !this.state.on
    });
  };

  render() {
    return (
      <div>
        <Light on={this.state.on}/>
        <LightSwitch onPress={this.press}/>
        <LightSwitch onPress={this.press}/>
      </div>
    );
  }
}

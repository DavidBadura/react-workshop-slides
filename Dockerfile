FROM node:11 as build

WORKDIR /var/build

COPY . .

RUN yarn install
RUN yarn build-dev

FROM nginx:1.15

COPY --from=build /var/build/dist/ /usr/share/nginx/html/dist/
COPY --from=build /var/build/index.html /usr/share/nginx/html/index.html

import React from 'react';

export default function BackgroundText({ children, color }) {
  const style = {
    bottom: 100,
    right: 100,
    position: 'absolute',
    textAlign: 'right',
    color: color,
    fontSize: 124,
    fontWeight: 'bold'
};

  return (
    <span style={style}>{children}</span>
  );
}

import React from 'react';
import logoWhite from '../assets/pv-logo-white.svg';
import logoBlack from '../assets/pv-logo.svg';

export default function Logo({ alternative = false }) {
  const style = {
    width: 200,
    bottom: 10,
    left: 10,
    position: 'absolute'
  };

  return (
    <img style={style} src={alternative ? logoBlack : logoWhite} />
  );
}

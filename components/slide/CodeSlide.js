import { Image, Slide } from 'spectacle';
import React from 'react';
import Logo from '../Logo';

export default function CodeSlide({imageHeading, image}) {
  return (
    <Slide bgColor="tertiary" progessColor="#888" controlColor="#888">
      <ImageHeading>
        {imageHeading}
      </ImageHeading>
      <Image src={image}/>
      <Logo alternative/>
    </Slide>
  );
}

const ImageHeading = ({ children }) => (
  <span
    style={{
      color: '#888',
      textAlign: 'left',
      fontWeight: 'bold'
    }}
    className="test"
    id="test"
  >
    {children}
  </span>
);

import { Slide, Text } from 'spectacle';
import React from 'react';
import Logo from '../Logo';
import BackgroundText from '../BackgroundText';

export default function PracticeSlide({children}) {
  return (
    <Slide bgColor="#39bbcf" progressColor="#888">
      <Text textColor="tertiary">{children}</Text>
      <BackgroundText color="#2793a4">Exercise</BackgroundText>
    </Slide>
  );
}

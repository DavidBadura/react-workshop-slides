import { Heading, Slide } from 'spectacle';
import React from 'react';
import Logo from '../Logo';

export default function StepSlide({children}) {
  return (
    <Slide>
      <Heading size={3} lineHeight={1} textColor="secondary">
        {children}
      </Heading>
      <Logo/>
    </Slide>
  );
}

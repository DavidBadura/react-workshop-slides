import { Slide } from 'spectacle';
import React from 'react';
import Logo from '../Logo';

export default function QuoteSlide({children}) {
  return (
    <Slide>
      <div className="blockquote-wrapper">
        <div className="blockquote">
          <h1>{children}</h1>
        </div>
      </div>
      <Logo/>
    </Slide>
  );
}

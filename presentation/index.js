// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  Deck,
  Heading,
  Image,
  List,
  ListItem,
  Slide,
  Text,
  Fill,
  Layout
} from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';
import Logo from '../components/Logo';
import QuoteSlide from '../components/slide/QuoteSlide';
import PracticeSlide from '../components/slide/PracticeSlide';
import HeroSlide from '../components/slide/HeroSlide';
import StepSlide from '../components/slide/StepSlide';
import CodeSlide from '../components/slide/CodeSlide';

const images = {
  profil: require('../assets/profil.jpg'),
  twitter: require('../assets/twitter.svg'),
  gitlab: require('../assets/gitlab.svg'),
  github: require('../assets/github.svg'),

  routerLogo: require('../assets/react-router.png'),

  size: require('../assets/size.png'),
  value: require('../assets/value.png'),
  virtual: require('../assets/virtual.png'),
  update: require('../assets/update.png'),
  flow: require('../assets/flow.png'),
  state: require('../assets/state.png'),
  cycle: require('../assets/cycle.png'),
  html: require('../assets/html.png'),
  components: require('../assets/components.png'),
  wtf: require('../assets/wtf.png'),
  joindin: require('../assets/joindin.png'),

  website1: require('../assets/website1.jpg'),
  website2: require('../assets/website2.jpg'),
  website3: require('../assets/website3.jpg'),
  website4: require('../assets/website4.jpg'),

  road: require('../assets/road.jpg'),
  ui: require('../assets/user-interface.jpg'),
  design: require('../assets/not-only-design.jpg'),
  factorio: require('../assets/factorio.jpg'),
  engine: require('../assets/engine.jpg'),
  scared: require('../assets/scared.jpg'),
  support: require('../assets/support.jpg'),
  nutshell: require('../assets/nutshell.jpg'),
  integration: require('../assets/integration.jpg'),
  hook: require('../assets/hook.jpg'),
  simple: require('../assets/simple.jpg'),
  qanda: require('../assets/qanda.jpg'),
  security: require('../assets/security.jpg'),
  routing: require('../assets/routing.jpg'),
  mobile: require('../assets/mobile.jpg'),

  jquery: require('../assets/jquery.png'),
  hello: require('../assets/hello-world.png'),
  jsx: require('../assets/jsx.png'),
  jumbotron: require('../assets/jumbotron.png'),
  jumbotron2: require('../assets/jumbotron2.png'),
  function: require('../assets/function.png'),
  switch: require('../assets/switch.png'),
  mixed: require('../assets/mixed.png'),
  controlled: require('../assets/controlled.png'),
  fragment: require('../assets/fragment.png'),
  fetch: require('../assets/fetch.png'),
  app: require('../assets/app.png'),
  default: require('../assets/default.png'),
  plugin: require('../assets/plugin.png'),
  index: require('../assets/index.png'),
  types: require('../assets/types.png'),
  error: require('../assets/error.png'),
  cors: require('../assets/cors.png'),
  native: require('../assets/native.png'),
  callback: require('../assets/callback.png'),
  hook_state: require('../assets/hook_state.png'),
  hook_effect: require('../assets/hook_effect.png')
};

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: '#333333',
    secondary: '#39bbcf',
    tertiary: '#efefef',
    quaternary: '#efefef',
    grey: '#ddd',
    red: '#c92a2a'
  },
  {
    primary: 'proxima-nova'
  }
);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck theme={theme} progress="number" showFullscreenControl={false}>
        <Slide bgColor="primary">
          <ContentHeading textColor="secondary" textSize={32} padding={5}>Willkommen!</ContentHeading>
          <Logo/>
        </Slide>
        <QuoteSlide>
          Wer bezeichnet sich als <strong>Backend&nbsp;Entwickler?</strong>
        </QuoteSlide>
        <QuoteSlide>
          Wer bezeichnet sich als <strong>Frontend&nbsp;Entwickler?</strong>
        </QuoteSlide>
        <QuoteSlide>
          Wer bezeichnet sich als <strong>Full&nbsp;Stack&nbsp;Entwickler?</strong>
        </QuoteSlide>
        <QuoteSlide>
          Wer bezeichnet sich als<br/>
          <strong>
            <del>Full&nbsp;Stack&nbsp;Entwickler</del>
          </strong><br/>
          <strong>Software&nbsp;Entwickler?</strong>
        </QuoteSlide>
        <Slide bgColor="primary">
          <Layout>
            <Fill>
              <Image src={images.profil} style={{ maxWidth: '70%' }}/>
            </Fill>
            <Fill>
              <Heading padding={5} textAlign="left" size={5} textColor="secondary">David Badura</Heading>
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                Software Entwickler<br/>bei der pro.volution GmbH
              </Text>
              <br/>
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                <FlexedRow>
                  <MiniLogo src={images.twitter}/>
                  <span>@davidbadura</span>
                </FlexedRow>
              </Text>
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                <FlexedRow>
                  <MiniLogo src={images.github}/>
                  <span>DavidBadura</span>
                </FlexedRow>
              </Text>
              <Text textAlign="left" textColor="tertiary" textSize={32} padding={5}>
                <FlexedRow>
                  <MiniLogo src={images.gitlab}/>
                  <span>DavidBadura</span>
                </FlexedRow>
              </Text>
            </Fill>
          </Layout>
          <Text textAlign="center" textColor="secondary" textSize={32} padding={50}>
            Loves ReactJS, Symfony & simplicity
          </Text>
          <Logo/>
        </Slide>
        <Slide bgColor="primary">
          <Heading textColor="tertiary" textSize={42} padding={5}>Einführung in</Heading>
          <ContentHeading textColor="secondary" textSize={32} padding={5}>ReactJS</ContentHeading>
          <Heading textColor="tertiary" textSize={42} padding={5}>Workshop Edition</Heading>
          <Logo/>
        </Slide>
        <Slide bgImage={images.road} bgDarken={0.5}>
          <ContentHeadingImage>
            Roadmap
          </ContentHeadingImage>
          <Logo/>
        </Slide>
        <Slide>
          <RealHeading>
            Roadmap
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Installation</ListItem>
            <ListItem>Was ist ReactJS?</ListItem>
            <ListItem>Aufgaben</ListItem>
            <ListItem>Noch mehr ReactJS</ListItem>
            <ListItem>Noch mehr Aufgaben</ListItem>
            <ListItem>Abspan</ListItem>
          </List>
          <Logo/>
        </Slide>
        <Slide>
          <RealHeading>
            Requirements
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Moderner Brower</ListItem>
            <ListItem>Node LTS</ListItem>
            <ListItem>IDE || Notepad++ || VIM</ListItem>
            <ListItem>https://github.com/DavidBadura/react-workshop</ListItem>
          </List>
          <Logo/>
        </Slide>
        <Slide bgImage={images.ui} bgDarken={0.2}>
          <ContentHeadingImage>
            User Interface
          </ContentHeadingImage>
          <Logo/>
        </Slide>
        <QuoteSlide>
          The user interface is one of the <strong>most important parts</strong> of any program
          because it determines <strong>how easily</strong> you can make the program to do what <strong>you want</strong>.
        </QuoteSlide>
        <Slide bgImage={images.design} bgDarken={0.4}>
          <ContentHeadingImage>
            Es geht nicht nur um's Design
          </ContentHeadingImage>
          <Logo/>
        </Slide>
        <Slide>
          <RealHeading>
            Es geht um
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>Design</ListItem>
            <ListItem>User Experience</ListItem>
            <ListItem>Anpassbarkeit</ListItem>
            <ListItem>Performanz</ListItem>
            <ListItem>Skalierbarkeit</ListItem>
            <ListItem>Optimierung</ListItem>
          </List>
          <Logo/>
        </Slide>
        <Slide>
          <img src={images.size}/>
          <Logo/>
        </Slide>
        <QuoteSlide>
          Because something is simple to you, <strong>doesn't mean</strong> it's for everyone.
        </QuoteSlide>
        <Slide bgImage={images.factorio} bgDarken={0.5}>
          <ContentHeadingImage>
            Wann wird ein Projekt Komplex?
          </ContentHeadingImage>
          <Logo/>
        </Slide>
        <Slide>
          <img src={images.wtf}/>
          <Logo/>
        </Slide>
        <Slide>
          <img src={images.value}/>
          <Logo/>
        </Slide>
        <Slide bgImage={images.support} bgDarken={0.6}>
          <ContentHeadingImage>
            Wie kann ReactJS hier unterstützen?
          </ContentHeadingImage>
          <Logo/>
        </Slide>
        <HeroSlide>
          ReactJS ist eine UI Library
        </HeroSlide>
        <HeroSlide>
          Library &ne; Framework
        </HeroSlide>
        <Slide bgImage={images.engine} bgDarken={0.5}>
          <ContentHeadingImage>
            "Engine" um mehrere Platformen zu bedienen
          </ContentHeadingImage>
          <Logo/>
        </Slide>
        <Slide>
          <List textColor="tertiary">
            <ListItem>React-DOM <small>(HTML)</small></ListItem>
            <ListItem>React-Native <small>(Android/iOS)</small></ListItem>
            <ListItem>React-Native-Windows <small>(Windows 10 SDK)</small></ListItem>
            <ListItem>React-360 <small>(VR mit WebGL/WebVR)</small></ListItem>
            <ListItem>React-TV <small>(WebOS/SmartTVs)</small></ListItem>
            <ListItem>React-PHP <small>(???)</small></ListItem>
          </List>
          <Logo/>
        </Slide>
        <Slide>
          <List textColor="tertiary">
            <ListItem>React-DOM <small>(HTML)</small> &larr;</ListItem>
            <ListItem>React-Native <small>(Android/iOS)</small></ListItem>
            <ListItem>React-Native-Windows <small>(Windows 10 SDK)</small></ListItem>
            <ListItem>React-360 <small>(VR mit WebGL/WebVR)</small></ListItem>
            <ListItem>React-TV <small>(WebOS/SmartTVs)</small></ListItem>
            <ListItem>
              <del>React-PHP</del>
            </ListItem>
          </List>
          <Logo/>
        </Slide>
        <HeroSlide>
          Wer verwendet es?
        </HeroSlide>
        <Slide>
          <List textColor="tertiary">
            <ListItem>Facebook</ListItem>
            <ListItem>Whatsapp</ListItem>
            <ListItem>Github</ListItem>
            <ListItem>Airbnb</ListItem>
            <ListItem>Netflix</ListItem>
            <ListItem>Google</ListItem>
          </List>
          <Logo/>
        </Slide>
        <HeroSlide>
          Wieso?
        </HeroSlide>
        <HeroSlide>
          Keep it simple
        </HeroSlide>
        <HeroSlide>
          Aber wie?
        </HeroSlide>
        <StepSlide>
          #1
        </StepSlide>
        <HeroSlide>
          Ohne DOM Manipulation
        </HeroSlide>

        <CodeSlide image={images.jquery} imageHeading="accordion.jquery.js"/>

        <StepSlide>
          #2
        </StepSlide>

        <HeroSlide>
          Alles ist eine Komponente
        </HeroSlide>

        <Slide>
          <List textColor="tertiary">
            <ListItem>Views</ListItem>
            <ListItem>Models</ListItem>
            <ListItem>Controllers</ListItem>
            <ListItem>Modules</ListItem>
            <ListItem>2-Way Binding</ListItem>
            <ListItem>Observer (Watcher)</ListItem>
            <ListItem>Templates</ListItem>
          </List>
          <Logo/>
        </Slide>

        <Slide>
          <List textColor="tertiary">
            <ListItem>
              <del>Views</del>
            </ListItem>
            <ListItem>
              <del>Models</del>
            </ListItem>
            <ListItem>
              <del>Controllers</del>
            </ListItem>
            <ListItem>
              <del>Modules</del>
            </ListItem>
            <ListItem>
              <del>2-Way Binding</del>
            </ListItem>
            <ListItem>
              <del>Observer (Watcher)</del>
            </ListItem>
            <ListItem>
              <del>Templates</del>
            </ListItem>
          </List>
          <Logo/>
        </Slide>

        <QuoteSlide>
          React is all about building <strong>reusable</strong> components.
          Since they are so <strong>encapsulated</strong>, components make code reuse, <strong>testing</strong>,
          and <strong>separation of concerns</strong> easy.
        </QuoteSlide>

        <Slide>
          <img src={images.website1}/>
          <Logo/>
        </Slide>

        <Slide>
          <img src={images.website2}/>
          <Logo/>
        </Slide>

        <Slide>
          <img src={images.website3}/>
          <Logo/>
        </Slide>

        <Slide>
          <img src={images.website4}/>
          <Logo/>
        </Slide>

        <HeroSlide>
          Das ist das Konzept...
        </HeroSlide>

        <CodeSlide image={images.hello} imageHeading="hello-world.js"/>

        <Slide bgImage={images.scared} bgDarken={0.5}>
          <ContentHeadingImage>
            HTML in Javascript?
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <HeroSlide>
          JSX
        </HeroSlide>

        <QuoteSlide>
          A Javascript XML based extension that compile <strong>just the template</strong> part of your code
          representation.
        </QuoteSlide>

        <CodeSlide image={images.hello} imageHeading="hello-world.js"/>
        <CodeSlide image={images.jsx} imageHeading="compiled.js"/>


        <QuoteSlide>
          We strongly believe that <strong>components</strong> are the right way
          to <strong>separate concerns</strong> rather then "templates" and "display logic".
        </QuoteSlide>

        <HeroSlide>
          Aber wieso?
        </HeroSlide>

        <CodeSlide image={images.jquery} imageHeading="accordion.jquery.js"/>

        <HeroSlide>
          Virtual DOM
        </HeroSlide>

        <QuoteSlide>
          An object tree that is a <strong>representation</strong> of the DOM.
        </QuoteSlide>

        <Slide>
          <img src={images.virtual}/>
          <Logo/>
        </Slide>
        <Slide>
          <img src={images.html}/>
          <Logo/>
        </Slide>
        <Slide>
          <img src={images.components}/>
          <Logo/>
        </Slide>
        <Slide>
          <img src={images.cycle}/>
          <Logo/>
        </Slide>
        <Slide>
          <img src={images.update}/>
          <Logo/>
        </Slide>
        <QuoteSlide>
          <strong>Javascript is fast</strong> and the DOM is slow!
        </QuoteSlide>
        <CodeSlide image={images.hello} imageHeading="hello-world.js"/>
        <HeroSlide>
          Props & State
        </HeroSlide>

        <HeroSlide>
          Props
        </HeroSlide>

        <Slide>
          <img src={images.flow}/>
          <Logo/>
        </Slide>

        <CodeSlide image={images.jumbotron} imageHeading="jumbotron.js"/>
        <CodeSlide image={images.jumbotron2} imageHeading="children-example.js"/>
        <CodeSlide image={images.function} imageHeading="function-example.js"/>
        <CodeSlide image={images.callback} imageHeading="callback-example.js"/>

        <HeroSlide>
          State
        </HeroSlide>

        <Slide>
          <img src={images.state}/>
          <Logo/>
        </Slide>

        <CodeSlide image={images.switch} imageHeading="switch.js"/>
        <CodeSlide image={images.mixed} imageHeading="mixed.js"/>
        <CodeSlide image={images.controlled} imageHeading="controlled.js"/>
        <CodeSlide image={images.fragment} imageHeading="fragment.js"/>

        <PracticeSlide>
          <p>
            Aufgabe 1 & 2
          </p>
        </PracticeSlide>

        <HeroSlide>
          Prop types & defaults
        </HeroSlide>

        <CodeSlide image={images.types} imageHeading="types.js"/>
        <CodeSlide image={images.default} imageHeading="default.js"/>

        <HeroSlide>
          Component Lifecycle
        </HeroSlide>

        <QuoteSlide>
          React provides <strong>hooks</strong>, methods that get called <strong>automatically at each point</strong> in
          the lifecycle.
        </QuoteSlide>

        <Slide>
          <RealHeading>
            Mounting
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>static getDerivedStateFromProps(props, state)</ListItem>
            <ListItem>constructor(props)</ListItem>
            <ListItem>render()</ListItem>
            <ListItem>componentDidMount()</ListItem>
          </List>
          <Logo/>
        </Slide>

        <Slide>
          <RealHeading>
            Updating
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>static getDerivedStateFromProps(props, state)</ListItem>
            <ListItem>shouldComponentUpdate(nextProps, nextState)</ListItem>
            <ListItem>render()</ListItem>
            <ListItem>componentDidUpdate()</ListItem>
          </List>
          <Logo/>
        </Slide>

        <Slide>
          <RealHeading>
            Unmounting
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>componentWillUnmount()</ListItem>
          </List>
          <Logo/>
        </Slide>

        <Slide>
          <RealHeading>
            Error Handling
          </RealHeading>
          <List textColor="tertiary">
            <ListItem>componentDidCatch(error, info)</ListItem>
            <ListItem>static getDerivedStateFromError(error)</ListItem>
          </List>
          <Logo/>
        </Slide>

        <CodeSlide image={images.fetch} imageHeading="movies.js"/>
        <CodeSlide image={images.error} imageHeading="error.js"/>

        <PracticeSlide>
          <p>
            Aufgabe 3, 4, 5 & 6
          </p>
        </PracticeSlide>

        <Slide bgImage={images.integration} bgDarken={0.5}>
          <ContentHeadingImage>
            Integration
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <CodeSlide image={images.index} imageHeading="integration.js"/>

        <HeroSlide>
          Symfony Encore
        </HeroSlide>

        <HeroSlide>
          Single-Page-App
        </HeroSlide>

        <HeroSlide>
          create-react-app
        </HeroSlide>

        <Slide bgImage={images.hook} bgDarken={0.5}>
          <ContentHeadingImage>
            Hooks
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <CodeSlide image={images.hook_state} imageHeading="hooks1.js"/>
        <CodeSlide image={images.hook_effect} imageHeading="hooks2.js"/>

        <Slide bgImage={images.integration} bgDarken={0.5}>
          <ContentHeadingImage>
            3-Party-Extension
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <CodeSlide image={images.plugin} imageHeading="jquery.plugin.js"/>

        <Slide bgImage={images.routing} bgDarken={0.5}>
          <ContentHeadingImage>
            Routing
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <CodeSlide image={images.routerLogo}/>
        <CodeSlide image={images.app} imageHeading="app.js"/>

        <Slide bgImage={images.security} bgDarken={0.5}>
          <ContentHeadingImage>
            Security
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <HeroSlide>
          Session
        </HeroSlide>

        <HeroSlide>
          API-Key
        </HeroSlide>

        <HeroSlide>
          JWT-Token
        </HeroSlide>

        <Slide bgImage={images.security} bgDarken={0.5}>
          <ContentHeadingImage>
            Cross-Origin Resource Sharing
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <HeroSlide>
          nelmio/cors-bundle
        </HeroSlide>

        <CodeSlide image={images.cors} imageHeading="nelmio_cors.yaml"/>

        <Slide bgImage={images.mobile} bgDarken={0.5}>
          <ContentHeadingImage>
            Mobile
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <HeroSlide>
          React-Native
        </HeroSlide>

        <CodeSlide image={images.native} imageHeading="app.js"/>

        <HeroSlide>
          expo.io
        </HeroSlide>

        <HeroSlide>
          Progressive-Web-App
        </HeroSlide>

        <Slide>
          <List textColor="tertiary">
            <ListItem>Status Code 200</ListItem>
            <ListItem>HTTPS</ListItem>
            <ListItem>Manifest Datei</ListItem>
            <ListItem>Offline fähig</ListItem>
            <ListItem>Service Worker</ListItem>
          </List>
        </Slide>

        <Slide bgImage={images.nutshell} bgDarken={0.5}>
          <ContentHeadingImage>
            in a nutshell
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <QuoteSlide>
          React introduces a new way to build <strong>scalable</strong> applications
          that can take you out of your <strong>comfort zone</strong>.
        </QuoteSlide>

        <QuoteSlide>
          You need to <strong>clean your mind</strong> about
          a lot of <strong>old "trues"</strong>.
        </QuoteSlide>

        <QuoteSlide>
          It's about <strong>deliver value</strong>.
          If something help you to make that.
          Why do you worry about write your <strong>HTML inside Javascript?</strong>
        </QuoteSlide>

        <Slide bgImage={images.simple} bgDarken={0.5}>
          <ContentHeadingImage>
            Das wars...
          </ContentHeadingImage>
          <Logo/>
        </Slide>

        <HeroSlide>
          Danke!
        </HeroSlide>

        <Slide>
          <Heading size={3} lineHeight={1} textColor="secondary">
            https://joind.in/talk/d6f2b
          </Heading>
          <img src={images.joindin}/>

          <Logo/>
        </Slide>

        <Slide bgImage={images.qanda} bgDarken={0.5}>
          <ContentHeadingImage>
            Q&A
          </ContentHeadingImage>
          <Logo/>
        </Slide>
      </Deck>
    );
  }
}

const RealHeading = ({ children }) => (
  <Heading size={3} lineHeight={1} textColor="secondary" textAlign="left">
    {children}
  </Heading>
);

class ContentHeadingImage extends React.Component {
  render() {
    const { children } = this.props;

    const background = {
      width: '150%',
      marginLeft: '-25%'
    };

    const text = {};

    return (
      <div style={background}>
        <Heading size={3} lineHeight={1} textColor="tertiary" style={text}>
          {children}
        </Heading>
      </div>
    );
  }
}

const ContentHeading = ({ children }) => (
  <Heading size={3} lineHeight={1} textColor="secondary">
    {children}
  </Heading>
);

class MiniLogo extends React.Component {
  render() {
    const { src } = this.props;

    const style = {
      margin: 0,
      marginRight: 10,
      display: 'inline'
    };

    return (
      <Image src={src} width={32} style={style}/>
    );
  }
}

const FlexedRow = ({ children }) => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center'
    }}
  >
    {children}
  </div>
);
